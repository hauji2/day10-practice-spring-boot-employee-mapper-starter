package com.afs.restapi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class CompanyResponse {
    private Long id;
    private String name;
    List<EmployeeResponse> employees = new ArrayList<>();
}
