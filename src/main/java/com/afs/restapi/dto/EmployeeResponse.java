package com.afs.restapi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor
@AllArgsConstructor
@Data
@SuperBuilder(toBuilder = true)
public class EmployeeResponse {
    private Long id;
    private String name;
    private Integer age;
    private String gender;
    private Long companyId;
}
