package com.afs.restapi.mapper;

import com.afs.restapi.dto.EmployeeRequest;
import com.afs.restapi.dto.EmployeeResponse;
import com.afs.restapi.entity.Employee;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(
        componentModel = "spring",
        injectionStrategy = InjectionStrategy.CONSTRUCTOR
)
public interface EmployeeMapper {
    Employee toEmployeeWithId(EmployeeRequest employeeRequest, Long id);

    @Mapping(target = "id", ignore = true)
    Employee toEmployee(EmployeeRequest employeeRequest);

    EmployeeResponse toEmployeeResponse(Employee employee);

    List<EmployeeResponse> toEmployeeResponses(List<Employee> employees);
}
