package com.afs.restapi.mapper;

import com.afs.restapi.dto.CompanyRequest;
import com.afs.restapi.dto.CompanyResponse;
import com.afs.restapi.entity.Company;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(
        componentModel = "spring",
        injectionStrategy = InjectionStrategy.CONSTRUCTOR,
        uses = {
                EmployeeMapper.class
        }
)
public interface CompanyMapper {

    @Mapping(source = "employees", target = "employees")
    CompanyResponse toCompanyResponse(Company company);

    @Mapping(source = "employees", target = "employees")
    List<CompanyResponse> toCompanyResponses(List<Company> companies);

    @Mapping(target = "employees", ignore = true)
    Company toCompany(CompanyRequest companyRequest, Long id);

    @Mapping(target = "id", ignore = true)
    Company toCompanyWithEmployees(CompanyRequest companyRequest);
}
