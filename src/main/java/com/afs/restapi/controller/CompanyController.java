package com.afs.restapi.controller;

import com.afs.restapi.dto.CompanyRequest;
import com.afs.restapi.dto.CompanyResponse;
import com.afs.restapi.dto.EmployeeResponse;
import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.mapper.CompanyMapper;
import com.afs.restapi.mapper.EmployeeMapper;
import com.afs.restapi.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("companies")
@RestController
public class CompanyController {

    @Autowired
    CompanyMapper companyMapper;

    @Autowired
    EmployeeMapper employeeMapper;

    @Autowired
    CompanyService companyService;

    @GetMapping
    public List<CompanyResponse> getAllCompanies() {
        List<Company> companies = companyService.findAll();
        return companyMapper.toCompanyResponses(companies);
    }

    @GetMapping(params = {"pageNumber", "pageSize"})
    public List<CompanyResponse> getCompaniesByPage(@RequestParam Integer pageNumber, @RequestParam Integer pageSize) {
        List<Company> companies = companyService.findByPage(pageNumber, pageSize);
        return companyMapper.toCompanyResponses(companies);
    }

    @GetMapping("/{id}")
    public CompanyResponse getCompanyById(@PathVariable Long id) {
        Company company = companyService.findById(id);
        return companyMapper.toCompanyResponse(company);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateCompany(@PathVariable Long id, @RequestBody CompanyRequest companyRequest) {
        Company company = companyMapper.toCompany(companyRequest, id);
        companyService.update(id, company);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCompany(@PathVariable Long id) {
        companyService.delete(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CompanyResponse createCompany(@RequestBody CompanyRequest companyRequest) {
        Company company = companyMapper.toCompanyWithEmployees(companyRequest);
        Company savedCompany = companyService.create(company);
        return companyMapper.toCompanyResponse(savedCompany);
    }

    @GetMapping("/{id}/employees")
    public List<EmployeeResponse> getEmployeesByCompanyId(@PathVariable Long id) {
        List<Employee> employees = companyService.findEmployeesByCompanyId(id);
        return employeeMapper.toEmployeeResponses(employees);
    }

}
