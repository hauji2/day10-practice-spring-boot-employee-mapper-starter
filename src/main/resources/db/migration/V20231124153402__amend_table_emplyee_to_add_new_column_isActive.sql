SELECT count(*)
INTO @exist
FROM information_schema.columns
WHERE table_schema = database()
  and COLUMN_NAME = 'isActive'
  AND table_name = 'employee';

set @query = IF(
            @exist <= 0,
            'ALTER TABLE `employee` ADD COLUMN isActive boolean AFTER salary',
            'select \'Column Exists\' status'
    );

prepare stmt from @query;
EXECUTE stmt;