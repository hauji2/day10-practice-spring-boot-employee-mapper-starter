SET @exist := (
    SELECT COUNT(*)
    FROM information_schema.columns
    WHERE table_schema = database()
    AND COLUMN_NAME = 'isActive'
    AND table_name = 'employee'
);

SET @query = CASE
    WHEN @exist > 0 THEN
        "ALTER TABLE `employee` CHANGE `isActive` `deleted` BOOLEAN"
    ELSE
        "SELECT 'Column does not Exist' as status"
END;

PREPARE stmt FROM @query;
EXECUTE stmt;